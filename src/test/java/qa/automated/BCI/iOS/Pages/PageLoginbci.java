package qa.automated.BCI.iOS.Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;



public class PageLoginbci {
	
	

	
	@FindBy(how = How.ID, using = "AppLoginViewController.txtRut")
	private WebElement Set_rut;
	
	@FindBy(how = How.ID, using = "AppLoginViewController.txtPass")
	private WebElement Set_pass;
	
	@FindBy(how = How.ID, using = "AppLoginViewController.btnLogin")
	private WebElement btn_login;

	@FindBy(how = How.ID, using = "AppLoginViewController.btnEye")
	private WebElement bnt_Ojo ;

	 
	@FindBy(how = How.ID, using = "AppLoginViewController.checkCircle")
	private WebElement click_aceptoCond ;

	@FindBy(how = How.NAME, using = "Allow")
	private WebElement btnALLOW;
	//Juan Carrasco
	@FindBy(how = How.NAME, using = "Juan Carrasco")
	private WebElement NombreCliente;

	/**
	 * 
	 * Getter and Setters
	 * 
	 * **/
	
	
	
	public WebElement getClick_aceptoCond() {
		return click_aceptoCond;
	}

	public WebElement getNombreCliente() {
		return NombreCliente;
	}

	public void setNombreCliente(WebElement nombreCliente) {
		NombreCliente = nombreCliente;
	}

	public WebElement getBtnALLOW() {
		return btnALLOW;
	}

	public void setBtnALLOW(WebElement btnALLOW) {
		this.btnALLOW = btnALLOW;
	}

	public WebElement getSet_pass() {
		return Set_pass;
	}

	public void setSet_pass(WebElement set_pass) {
		Set_pass = set_pass;
	}

	public WebElement getBtn_login() {
		return btn_login;
	}

	public void setBtn_login(WebElement btn_login) {
		this.btn_login = btn_login;
	}

	public WebElement getSet_rut() {
		return Set_rut;
	}

	public void setSet_rut(WebElement set_rut) {
		Set_rut = set_rut;
	}

	public void setClick_aceptoCond(WebElement click_aceptoCond) {
		this.click_aceptoCond = click_aceptoCond;
	}
	public WebElement getBnt_Ojo() {
		return bnt_Ojo;
	}

	public void setBnt_Ojo(WebElement bnt_Ojo) {
		this.bnt_Ojo = bnt_Ojo;
	}

	
	/**
	 * Metodos**/
	

	
	public void CheckAcept() {
		click_aceptoCond.click();
	}
	
	public boolean VerActivo() {
		boolean res = false;
		try {
			res = bnt_Ojo.isDisplayed();
		} catch (Exception e) {
			res = false;
		}

		return res;
	}

	public void ClickOjoVer() {
		bnt_Ojo.click();
	}
	
	public void clickCampoRut() {
		Set_rut.click();
		
	}
	public void ingresarRUT(String Rut) throws InterruptedException {

		Set_rut.clear();
		Set_rut.sendKeys(Rut);
		// driver.navigate().back();

	}
	
	public void ingresarClave(String pass) throws InterruptedException {

		Set_pass.clear();
		Set_pass.sendKeys(pass);
	}

	public String obtenerRut() {
		String Rut = "";
		Rut = Set_rut.getText();
		return Rut;

	}
	
	public boolean validarRut(String rut) {

		boolean validacion = true;
		try {
			rut = rut.toUpperCase();
			rut = rut.replaceAll(".", "");
			rut = rut.replace("-", "");
			int rutAux = Integer.parseInt(rut.substring(0, rut.length() - 1));

			char dv = rut.charAt(rut.length() - 1);

			int m = 0, s = 1;
			for (; rutAux != 0; rutAux /= 10) {
				s = (s + rutAux % 10 * (9 - m++ % 6)) % 11;
			}
			if (dv == (char) (s != 0 ? s + 47 : 75)) {
				validacion = false;
			}

		} catch (java.lang.NumberFormatException e) {
		} catch (Exception e) {
		}
		return validacion;
	}
	public void botonLogin() {
		btn_login.click();
	}
	
	public boolean btnLoginActivo() {
		boolean res = false;
		try {
			res = btn_login.isDisplayed();
		} catch (Exception e) {
			res = false;
		}

		return res;
	}
	
	public void BtnDialogoPermitir() {
		btnALLOW.click();
	}
	
	public boolean NombreClienteAct() {
		return NombreCliente.isDisplayed();
		
	}
}


