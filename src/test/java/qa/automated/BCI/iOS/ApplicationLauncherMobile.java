package qa.automated.BCI.iOS;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.ios.IOSElement;
import io.appium.java_client.remote.MobileCapabilityType;
import lombok.extern.slf4j.Slf4j;
import qa.automated.BCI.iOS.AppiumServer.AppiumServerJava;
import qa.automated.BCI.iOS.Pages.PageLoginbci;
import qa.automated.BCI.iOS.Runnings.RunningPageLoginBCI;
import qa.automated.BCI.iOS.properties.PropertiesInit;



@Slf4j
@FindBy
@RunWith(Suite.class)

@SuiteClasses({RunningPageLoginBCI.class})


public class ApplicationLauncherMobile {

	public static AppiumServerJava appium;
	public static AppiumDriver<IOSElement> driver;
	public static DesiredCapabilities capabilities;
	

    public static PageLoginbci OBFperfilpagelogins;

	
	public static PropertiesInit properties;

	
	public ApplicationLauncherMobile() {
		super();
		log.info("run 1 ¨***************************");
		
		// TODO Auto-generated constructor stub
	}

	public static DesiredCapabilities getDriverConfig() {
   

		capabilities = new DesiredCapabilities();

		// Nombre de dispositivo movil o emulador donde se ejecutará
				 capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME,"Appium");
				 capabilities.setCapability("platformName", "iOS");
				 capabilities.setCapability("deviceName", "iPhone X"); 
//				 capabilities.setCapability("bundleID", "3C8C7D85-5AB0-45C0-9126-B91180930B31");
				 capabilities.setCapability("automationName", "XCUITest");
				 ///Users/ivanmunozm./Library/Developer/CoreSimulator/Devices/83972866-3140-4540-9D4E-CCCDCD279F5D/data/Containers/Bundle/Application/04FC38C3-2BA1-4C64-9A4A-5CF7A3FAF
				capabilities.setCapability("app","/Users/ivanmunozm./Library/Developer/CoreSimulator/Devices/83972866-3140-4540-9D4E-CCCDCD279F5D/data/Containers/Bundle/Application/04FC38C3-2BA1-4C64-9A4A-5CF7A3FAFBE0/AppPersonasBci.app");
		// Bloquea teclado de dispositivo para ingresar datos sin perder foco
//			     capabilities.setCapability("unicodeKeyboard", true);
//				 capabilities.setCapability("resetKeyboard", true);
//				 capabilities.chrome();
		// Tiempo en segundos antes de esperar a cerrar aplicacion por inactividad, para este caso se configuro 60min
//				 capabilities.setCapability("newCommandTimeout", 1800); //60min
				 capabilities.setCapability("newCommandTimeout", 1200);
				 capabilities.setCapability("noReset", false);
				 return capabilities;

	}
	
	
	@BeforeClass
	public static void setUp() throws MalformedURLException {
		
		properties = new PropertiesInit();
		// ******************************starter
		// appium***************************************
		log.info("run 1 ¨***************************");		appium = new AppiumServerJava();
		appium.startServer();

		log.info("run 2¨***************************");		// ****************************************************************************

		driver = new AppiumDriver<IOSElement>(new URL(properties.getUrlAppium()), getDriverConfig());
		driver.manage().timeouts().implicitlyWait(80, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(120, TimeUnit.SECONDS);
//		driver.navigate().back();
	
		// *******************************Paginacion***************************************************************
	
		OBFperfilpagelogins = PageFactory.initElements(driver, PageLoginbci.class);
		
		// ****************************Paginacion***********************
	}

	@AfterClass
	public static void finish() {
		System.out.println("************************************************");
		System.out.println("**   Finaliza ciclo de pruebas automatizadas  **");
		System.out.println("************************************************");
		System.out.println();
		driver.quit();
		appium.stopServer();
	
	}

}
