package qa.automated.BCI.iOS.Definitions;

import static org.junit.Assert.assertTrue;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import qa.automated.BCI.iOS.ApplicationLauncherMobile;
import qa.automated.BCI.iOS.Genericos.UsoCom;






public class BCI_PageLogins {
	
	
	@When("^ingresa rut \"(.*?)\"\\.$")
	public void ingresa_rut(String arg1) throws Throwable {
		ApplicationLauncherMobile.OBFperfilpagelogins.clickCampoRut();
		boolean btn = ApplicationLauncherMobile.OBFperfilpagelogins.VerActivo();
		assertTrue(btn);
		Thread.sleep(2000);
	    ApplicationLauncherMobile.OBFperfilpagelogins.ClickOjoVer();
	    ApplicationLauncherMobile.OBFperfilpagelogins.ingresarRUT(arg1);

	}

	@Then("^valido rut ingresado con puntos y guiones\\.$")
	public void valido_rut_ingresado_con_puntos_y_guiones() throws Throwable {
		String rut = ApplicationLauncherMobile.OBFperfilpagelogins.obtenerRut();
		boolean resultado = ApplicationLauncherMobile.OBFperfilpagelogins.validarRut(rut);
        assertTrue(resultado);
		
	}

	@When("^Ingresar letras campo rut \"(.*?)\"\\.$")
	public void ingresar_letras_campo_rut(String letra) throws Throwable {
		ApplicationLauncherMobile.OBFperfilpagelogins.ingresarRUT(letra);
	}

	@Then("^Comprobar que no ingresa letras\\.$")
	public void comprobar_que_no_ingresa_letras() throws Throwable {
		String rut = ApplicationLauncherMobile.OBFperfilpagelogins.obtenerRut();
		assertTrue("Rut no permite letras correcto", rut.equalsIgnoreCase("") || UsoCom.isNumeric(rut));
	}

	@When("^Ingresar letra k campo rut \"(.*?)\"\\.$")
	public void ingresar_letra_k_campo_rut(String letra) throws Throwable {
		ApplicationLauncherMobile.OBFperfilpagelogins.ingresarRUT(letra);
	}

	@Then("^Comprobar que ingresa letra correctamente\\.$")
	public void comprobar_que_ingresa_letra_correctamente() throws Throwable {
		String rut = ApplicationLauncherMobile.OBFperfilpagelogins.obtenerRut();
		assertTrue("Rut permite letra K correcto", rut.equalsIgnoreCase("k") || rut.equalsIgnoreCase("K"));

	}

	@When("^ingresa rut invalido sin digito verificador\"(.*?)\"\\.$")
	public void ingresa_rut_invalido_sin_digito_verificador(String arg1) throws Throwable {
		ApplicationLauncherMobile.OBFperfilpagelogins.ingresarRUT(arg1);
		ApplicationLauncherMobile.OBFperfilpagelogins.botonLogin();
	}

	@Then("^Se valida ingreso de rut con menos carcartes y sin digito verificador\\.$")
	public void se_valida_ingreso_de_rut_con_menos_carcartes_y_sin_digito_verificador() throws Throwable {
	
	}

	@When("^ingreso en campo clave \"(.*?)\" con cinco digitos\\.$")
	public void ingreso_en_campo_clave_con_cinco_digitos(String arg1) throws Throwable {
	  ApplicationLauncherMobile.OBFperfilpagelogins.ingresarClave(arg1);
	}

	@Then("^Valido ingreso de clave con largo de clave invalida\\.$")
	public void valido_ingreso_de_clave_con_largo_de_clave_invalida() throws Throwable {
	  
	}

	@When("^ingreso rut \"(.*?)\" y clave \"(.*?)\"\\.\\.$")
	public void ingreso_rut_y_clave(String arg1, String arg2) throws Throwable {
		ApplicationLauncherMobile.OBFperfilpagelogins.ingresarRUT(arg1);
		ApplicationLauncherMobile.OBFperfilpagelogins.ingresarClave(arg2);
		ApplicationLauncherMobile.OBFperfilpagelogins.CheckAcept();
	}

	@Then("^Se valida ingreso de clave y rut validos de forma exitosa\\.\\.$")
	public void se_valida_ingreso_de_clave_y_rut_validos_de_forma_exitosa() throws Throwable {
		
		ApplicationLauncherMobile.OBFperfilpagelogins.btnLoginActivo();
		Thread.sleep(2000);
		ApplicationLauncherMobile.OBFperfilpagelogins.ClickOjoVer();
		ApplicationLauncherMobile.OBFperfilpagelogins.botonLogin();
	    ApplicationLauncherMobile.OBFperfilpagelogins.BtnDialogoPermitir();
	    Thread.sleep(2000);
	    ApplicationLauncherMobile.OBFperfilpagelogins.NombreClienteAct();
	    
		
		
		
	}



}
