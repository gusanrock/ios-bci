package qa.automated.BCI.iOS.Runnings;

import java.io.File;

import org.junit.AfterClass;
import org.junit.runner.RunWith;

import com.cucumber.listener.Reporter;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(    
		features = {"src/test/java/qa/automated/BCI/iOS/Feature"},
	     glue = "qa/automated/BCI/iOS/Definitions",
         plugin={"pretty:target/reportes/pretty/pretty.txt",
        		 "json:target/reportes/json/report.json",
                 "junit:target/reportes/junit/junit.xml",
                 "usage:target/reportes/usage/usage.json",
                  "pretty", "html:target/htmlreports",
                "com.cucumber.listener.ExtentCucumberFormatter:target/cucumber-reports/report.html",
                 "rerun:target/reportes/rerun/rerun.txt"},
        		 monochrome = true,
            tags="@BCI_PageLogins"
            )



public class RunningPageLoginBCI {

//	@AfterClass
//    public static void writeExtentReport() {
//        Reporter.loadXMLConfig(new File("config/report.xml"));
//    
//    }
}
