package qa.automated.BCI.iOS.AppiumServer;

import java.io.IOException;
import java.net.ServerSocket;

import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import io.appium.java_client.service.local.flags.GeneralServerFlag;
import qa.automated.BCI.iOS.ApplicationLauncherMobile;


public class AppiumServerJava {

	private AppiumDriverLocalService service;

	/**
	 * Configuracion e iniciacion de servidor Appium
	 * 
	 * @return Voids
	 **/
	public void startServer() {
		// Configuracion appium server
		AppiumServiceBuilder builder = new AppiumServiceBuilder();
	//builder.withIPAddress("127.0.0.1");
		builder.usingPort(4723);
		builder.withCapabilities(ApplicationLauncherMobile.getDriverConfig());
		builder.withArgument(GeneralServerFlag.SESSION_OVERRIDE);
		builder.withArgument(GeneralServerFlag.LOG_LEVEL, "error");

		// Inicia servicio appium server
		service = AppiumDriverLocalService.buildService(builder);
		service.start();
	}

	/**
	 * Detiene el servidor Appium
	 * 
	 * @return Void
	 **/
	public void stopServer() {
		service.stop();
	}

	/**
	 * Configuracion de DesiredCapabilities que se entregaran a appium para que
	 * se ejecute
	 * 
	 * @return DesiredCapabilities
	 **/

	/**
	 * Comprueba si puerto esta en uso, si est� en uso lo libera, no realiza
	 * nada en caso contrario
	 * 
	 * @param int
	 *            port
	 * 
	 * @return true esta en uso el puerto, false no esta en uso o se libero
	 *         puerto
	 **/
	public static boolean checkIfServerIsRunnning(int port) {
		boolean isServerRunning = false;
		ServerSocket serverSocket;
		try {
			serverSocket = new ServerSocket(port);
			serverSocket.close();
		} catch (IOException e) {
			isServerRunning = true;
		} finally {
			serverSocket = null;
		}
		return isServerRunning;
	}

}
