package qa.automated.BCI.iOS.properties;

import java.io.InputStream;
import java.util.Properties;

import lombok.extern.slf4j.Slf4j;


@Slf4j
public class PropertiesInit {

	private String urlAppium;


	public PropertiesInit() {
		Properties parametros = new Properties();
		String propFileName = "config/config.properties";
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);

		try {
			parametros.load(inputStream);

			urlAppium = parametros.getProperty("urlAppium");
			
		
		} catch (Exception e) {
			log.error(String.valueOf(e));
		}

	}


	public String getUrlAppium() {
		return urlAppium;
	}


	public void setUrlAppium(String urlAppium) {
		this.urlAppium = urlAppium;
	}

}
