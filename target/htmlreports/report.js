$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("PageLoginbci.feature");
formatter.feature({
  "line": 2,
  "name": "Validacion de campos rut y clave he ingresar a pantalla de usuario",
  "description": "",
  "id": "validacion-de-campos-rut-y-clave-he-ingresar-a-pantalla-de-usuario",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@BCI_PageLogins"
    }
  ]
});
formatter.scenarioOutline({
  "line": 4,
  "name": "validacion seteo por puntos y guion",
  "description": "",
  "id": "validacion-de-campos-rut-y-clave-he-ingresar-a-pantalla-de-usuario;validacion-seteo-por-puntos-y-guion",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 5,
  "name": "ingresa rut \u003crut\u003e.",
  "keyword": "When "
});
formatter.step({
  "line": 6,
  "name": "valido rut ingresado con puntos y guiones.",
  "keyword": "Then "
});
formatter.examples({
  "line": 8,
  "name": "",
  "description": "",
  "id": "validacion-de-campos-rut-y-clave-he-ingresar-a-pantalla-de-usuario;validacion-seteo-por-puntos-y-guion;",
  "rows": [
    {
      "cells": [
        "rut"
      ],
      "line": 9,
      "id": "validacion-de-campos-rut-y-clave-he-ingresar-a-pantalla-de-usuario;validacion-seteo-por-puntos-y-guion;;1"
    },
    {
      "cells": [
        "\"100423235\""
      ],
      "line": 10,
      "id": "validacion-de-campos-rut-y-clave-he-ingresar-a-pantalla-de-usuario;validacion-seteo-por-puntos-y-guion;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 10,
  "name": "validacion seteo por puntos y guion",
  "description": "",
  "id": "validacion-de-campos-rut-y-clave-he-ingresar-a-pantalla-de-usuario;validacion-seteo-por-puntos-y-guion;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 1,
      "name": "@BCI_PageLogins"
    }
  ]
});
formatter.step({
  "line": 5,
  "name": "ingresa rut \"100423235\".",
  "matchedColumns": [
    0
  ],
  "keyword": "When "
});
formatter.step({
  "line": 6,
  "name": "valido rut ingresado con puntos y guiones.",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "100423235",
      "offset": 13
    }
  ],
  "location": "BCI_PageLogins.ingresa_rut(String)"
});
formatter.result({
  "duration": 147414343029,
  "error_message": "org.openqa.selenium.WebDriverException: An unknown server-side error occurred while processing the command. Original error: Could not proxy command to remote server. Original error: Error: connect ECONNREFUSED 127.0.0.1:8100 (WARNING: The server did not provide any stacktrace information)\nCommand duration or timeout: 0 milliseconds\nBuild info: version: \u00273.6.0\u0027, revision: \u00276fbf3ec767\u0027, time: \u00272017-09-27T15:28:36.4Z\u0027\nSystem info: host: \u0027Gusano.local\u0027, ip: \u0027fe80:0:0:0:fe:951f:e678:e009%en0\u0027, os.name: \u0027Mac OS X\u0027, os.arch: \u0027x86_64\u0027, os.version: \u002710.13.5\u0027, java.version: \u00271.8.0_171\u0027\nDriver info: io.appium.java_client.AppiumDriver\nCapabilities [{app\u003d/Users/ivanmunozm./Library/Developer/CoreSimulator/Devices/83972866-3140-4540-9D4E-CCCDCD279F5D/data/Containers/Bundle/Application/04FC38C3-2BA1-4C64-9A4A-5CF7A3FAFBE0/AppPersonasBci.app, networkConnectionEnabled\u003dfalse, noReset\u003dfalse, databaseEnabled\u003dfalse, deviceName\u003diPhone X, platform\u003dMAC, newCommandTimeout\u003d1200, webStorageEnabled\u003dfalse, locationContextEnabled\u003dfalse, automationName\u003dXCUITest, browserName\u003d, takesScreenshot\u003dtrue, javascriptEnabled\u003dtrue, udid\u003dA47EEC44-BF06-49FD-B5A8-070E1DBCC5B9, platformName\u003dMAC}]\nSession ID: ae2985ad-d1d0-45a7-a7eb-6e610ee04616\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(NativeConstructorAccessorImpl.java:62)\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)\n\tat java.lang.reflect.Constructor.newInstance(Constructor.java:423)\n\tat org.openqa.selenium.remote.ErrorHandler.createThrowable(ErrorHandler.java:214)\n\tat org.openqa.selenium.remote.ErrorHandler.throwIfResponseFailed(ErrorHandler.java:166)\n\tat org.openqa.selenium.remote.http.JsonHttpResponseCodec.reconstructValue(JsonHttpResponseCodec.java:40)\n\tat org.openqa.selenium.remote.http.AbstractHttpResponseCodec.decode(AbstractHttpResponseCodec.java:82)\n\tat org.openqa.selenium.remote.http.AbstractHttpResponseCodec.decode(AbstractHttpResponseCodec.java:45)\n\tat org.openqa.selenium.remote.HttpCommandExecutor.execute(HttpCommandExecutor.java:164)\n\tat io.appium.java_client.remote.AppiumCommandExecutor.execute(AppiumCommandExecutor.java:89)\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:586)\n\tat io.appium.java_client.DefaultGenericMobileDriver.execute(DefaultGenericMobileDriver.java:46)\n\tat io.appium.java_client.AppiumDriver.execute(AppiumDriver.java:1)\n\tat io.appium.java_client.HasSessionDetails.getSessionDetails(HasSessionDetails.java:38)\n\tat io.appium.java_client.HasSessionDetails.getSessionDetail(HasSessionDetails.java:55)\n\tat io.appium.java_client.HasSessionDetails.getAutomationName(HasSessionDetails.java:71)\n\tat io.appium.java_client.DefaultGenericMobileDriver.toString(DefaultGenericMobileDriver.java:157)\n\tat io.appium.java_client.AppiumDriver.toString(AppiumDriver.java:1)\n\tat java.util.Formatter$FormatSpecifier.printString(Formatter.java:2886)\n\tat java.util.Formatter$FormatSpecifier.print(Formatter.java:2763)\n\tat java.util.Formatter.format(Formatter.java:2520)\n\tat java.util.Formatter.format(Formatter.java:2455)\n\tat java.lang.String.format(String.java:2940)\n\tat org.openqa.selenium.remote.RemoteWebElement.setFoundBy(RemoteWebElement.java:63)\n\tat org.openqa.selenium.remote.RemoteWebDriver.setFoundBy(RemoteWebDriver.java:372)\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:365)\n\tat io.appium.java_client.DefaultGenericMobileDriver.findElement(DefaultGenericMobileDriver.java:62)\n\tat io.appium.java_client.AppiumDriver.findElement(AppiumDriver.java:1)\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElementById(RemoteWebDriver.java:402)\n\tat io.appium.java_client.DefaultGenericMobileDriver.findElementById(DefaultGenericMobileDriver.java:70)\n\tat io.appium.java_client.AppiumDriver.findElementById(AppiumDriver.java:1)\n\tat org.openqa.selenium.By$ById.findElement(By.java:218)\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:348)\n\tat io.appium.java_client.DefaultGenericMobileDriver.findElement(DefaultGenericMobileDriver.java:58)\n\tat io.appium.java_client.AppiumDriver.findElement(AppiumDriver.java:1)\n\tat org.openqa.selenium.support.pagefactory.DefaultElementLocator.findElement(DefaultElementLocator.java:69)\n\tat org.openqa.selenium.support.pagefactory.internal.LocatingElementHandler.invoke(LocatingElementHandler.java:38)\n\tat com.sun.proxy.$Proxy16.sendKeys(Unknown Source)\n\tat qa.automated.BCI.iOS.Pages.PageLoginbci.ingresarRUT(PageLoginbci.java:131)\n\tat qa.automated.BCI.iOS.Definitions.BCI_PageLogins.ingresa_rut(BCI_PageLogins.java:25)\n\tat ✽.When ingresa rut \"100423235\".(PageLoginbci.feature:5)\n",
  "status": "failed"
});
formatter.match({
  "location": "BCI_PageLogins.valido_rut_ingresado_con_puntos_y_guiones()"
});
formatter.result({
  "status": "skipped"
});
formatter.scenarioOutline({
  "line": 12,
  "name": "Validar que no permita letras en campo rut",
  "description": "",
  "id": "validacion-de-campos-rut-y-clave-he-ingresar-a-pantalla-de-usuario;validar-que-no-permita-letras-en-campo-rut",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 13,
  "name": "Ingresar letras campo rut \u003cletras\u003e.",
  "keyword": "When "
});
formatter.step({
  "line": 14,
  "name": "Comprobar que no ingresa letras.",
  "keyword": "Then "
});
formatter.examples({
  "line": 16,
  "name": "",
  "description": "",
  "id": "validacion-de-campos-rut-y-clave-he-ingresar-a-pantalla-de-usuario;validar-que-no-permita-letras-en-campo-rut;",
  "rows": [
    {
      "cells": [
        "letras"
      ],
      "line": 17,
      "id": "validacion-de-campos-rut-y-clave-he-ingresar-a-pantalla-de-usuario;validar-que-no-permita-letras-en-campo-rut;;1"
    },
    {
      "cells": [
        "\"abcd\""
      ],
      "line": 18,
      "id": "validacion-de-campos-rut-y-clave-he-ingresar-a-pantalla-de-usuario;validar-que-no-permita-letras-en-campo-rut;;2"
    },
    {
      "cells": [
        "\"DFGH\""
      ],
      "line": 19,
      "id": "validacion-de-campos-rut-y-clave-he-ingresar-a-pantalla-de-usuario;validar-que-no-permita-letras-en-campo-rut;;3"
    },
    {
      "cells": [
        "\"  $%\u0026\""
      ],
      "line": 20,
      "id": "validacion-de-campos-rut-y-clave-he-ingresar-a-pantalla-de-usuario;validar-que-no-permita-letras-en-campo-rut;;4"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 18,
  "name": "Validar que no permita letras en campo rut",
  "description": "",
  "id": "validacion-de-campos-rut-y-clave-he-ingresar-a-pantalla-de-usuario;validar-que-no-permita-letras-en-campo-rut;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 1,
      "name": "@BCI_PageLogins"
    }
  ]
});
formatter.step({
  "line": 13,
  "name": "Ingresar letras campo rut \"abcd\".",
  "matchedColumns": [
    0
  ],
  "keyword": "When "
});
formatter.step({
  "line": 14,
  "name": "Comprobar que no ingresa letras.",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "abcd",
      "offset": 27
    }
  ],
  "location": "BCI_PageLogins.ingresar_letras_campo_rut(String)"
});
